import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.dockerCommand
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.gradle
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2021.2"

project {

    buildType(Build_2)
    buildType(Build)
}

object Build : BuildType({
    name = "Build"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        gradle {
            name = "build JAR"
            tasks = "clean build"
            enableDebug = true
            param("org.jfrog.artifactory.selectedDeployableServer.useM2CompatiblePatterns", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.publishBuildInfo", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.defaultModuleVersionConfiguration", "GLOBAL")
            param("org.jfrog.artifactory.selectedDeployableServer.gitReleaseBranchNamePrefix", "REL-BRANCH-")
            param("org.jfrog.artifactory.selectedDeployableServer.urlId", "0")
            param("org.jfrog.artifactory.selectedDeployableServer.enableReleaseManagement", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.resolvingRepo", "null")
            param("org.jfrog.artifactory.selectedDeployableServer.publishMavenDescriptors", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.publishIvyDescriptors", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.deployArtifacts", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.activateGradleIntegration", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.targetRepo", "Test")
        }
        dockerCommand {
            name = "build image"
            enabled = false
            commandType = build {
                source = file {
                    path = "Dockerfile"
                }
                namesAndTags = "omerh-docker-local.jfrog.io/docker-local:teamcity-build-1.0"
                commandArgs = "--pull"
            }
        }
        step {
            name = "push image"
            type = "ArtifactoryDocker"
            enabled = false
            param("org.jfrog.artifactory.selectedDeployableServer.publishBuildInfo", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.dockerImageName", "omerh-docker-local.jfrog.io/docker-local:teamcity-build-1.0")
            param("org.jfrog.artifactory.selectedDeployableServer.dockerCommand", "PUSH")
            param("org.jfrog.artifactory.selectedDeployableServer.urlId", "0")
            param("org.jfrog.artifactory.selectedDeployableServer.envVarsExcludePatterns", "*password*,*secret*")
            param("org.jfrog.artifactory.selectedDeployableServer.resolvingRepo", "bower")
            param("org.jfrog.artifactory.selectedDeployableServer.targetRepo", "docker-local")
        }
    }

    triggers {
        vcs {
        }
    }
})

object Build_2 : BuildType({
    name = "Build (1)"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        gradle {
            name = "build JAR"
            tasks = "clean build"
            param("org.jfrog.artifactory.selectedDeployableServer.useM2CompatiblePatterns", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.publishBuildInfo", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.defaultModuleVersionConfiguration", "GLOBAL")
            param("org.jfrog.artifactory.selectedDeployableServer.gitReleaseBranchNamePrefix", "REL-BRANCH-")
            param("org.jfrog.artifactory.selectedDeployableServer.urlId", "0")
            param("org.jfrog.artifactory.selectedDeployableServer.enableReleaseManagement", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.envVarsExcludePatterns", "*password*,*secret*")
            param("org.jfrog.artifactory.selectedDeployableServer.resolvingRepo", "null")
            param("org.jfrog.artifactory.selectedDeployableServer.publishMavenDescriptors", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.publishIvyDescriptors", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.deployArtifacts", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.activateGradleIntegration", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.targetRepo", "Test")
        }
        dockerCommand {
            name = "build image"
            enabled = false
            commandType = build {
                source = file {
                    path = "Dockerfile"
                }
                namesAndTags = "omerh-docker-local.jfrog.io/docker-local:teamcity-build-1.0"
                commandArgs = "--pull"
            }
        }
        step {
            name = "push image"
            type = "ArtifactoryDocker"
            enabled = false
            param("org.jfrog.artifactory.selectedDeployableServer.publishBuildInfo", "true")
            param("org.jfrog.artifactory.selectedDeployableServer.dockerImageName", "omerh-docker-local.jfrog.io/docker-local:teamcity-build-1.0")
            param("org.jfrog.artifactory.selectedDeployableServer.dockerCommand", "PUSH")
            param("org.jfrog.artifactory.selectedDeployableServer.urlId", "0")
            param("org.jfrog.artifactory.selectedDeployableServer.envVarsExcludePatterns", "*password*,*secret*")
            param("org.jfrog.artifactory.selectedDeployableServer.resolvingRepo", "bower")
            param("org.jfrog.artifactory.selectedDeployableServer.targetRepo", "docker-local")
        }
    }

    triggers {
        vcs {
        }
    }
})
